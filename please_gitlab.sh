#! /usr/bin/env bash

readonly SCRIPT_NAME=$(basename $0)
readonly SCRIPT_DIRECTORY=$(dirname $0)

# style
readonly STYLE_RESET=$(tput sgr0)
readonly STYLE_REVERSE=$(tput rev)
readonly STYLE_BOLD=$(tput bold)

# foreground
readonly FG_BLACK=$(tput setaf 0)
readonly FG_RED=$(tput setaf 1)
readonly FG_GREEN=$(tput setaf 2)
readonly FG_YELLOW=$(tput setaf 3)
readonly FG_BLUE=$(tput setaf 4)
readonly FG_MAGENTA=$(tput setaf 5)
readonly FG_CYAN=$(tput setaf 6)
readonly FG_WHITE=$(tput setaf 7)

# background
readonly BG_BLACK=$(tput setab 0)
readonly BG_RED=$(tput setab 1)
readonly BG_GREEN=$(tput setab 2)
readonly BG_YELLOW=$(tput setab 3)
readonly BG_BLUE=$(tput setab 4)
readonly BG_MAGENTA=$(tput setab 5)
readonly BG_CYAN=$(tput setab 6)
readonly BG_WHITE=$(tput setab 7)

readonly REMOTE='origin'

createMergeRequest() {
    if [ $# -lt 4 ]
    then
        printf "
${SCRIPT_NAME} ${FUNCNAME[0]} ${FG_YELLOW}branch milestone title description label[,label]${STYLE_RESET}
"

        return 1
    fi

    local branch="$1"
    shift
    local milestone="$1"
    shift
    local title="$1"
    shift
    local description="$1"
    shift

    IFS=','
    for label in $*
    do
        local optionLabels+=" -o merge_request.label='$label'"
    done
    IFS=' '

    echo git push --force -o merge_request.create \
        -o merge_request.title="'$title'" \
        -o merge_request.description="'$description'" \
        -o merge_request.milestone="'$milestone'" \
        $optionLabels \
        $REMOTE $branch
}

tryCreateMergeRequest() {
    createMergeRequest some-branch 'sprint 26' 'hello world title' 'hello world description' 'need review,awesome,backend'
}

# Display the source code of this file
howItWorks() {
    if [ $# -lt 1 ]
    then
        less "$0"

        return
    fi

    less --pattern="$@" "$0"
}

# List all functions that do not begin with an underscore _
_listAvailableFunctions() {
    cat $0 | grep -E '^[a-z]+[a-zA-Z0-9_]*\(\) \{$' | sed 's#() {$##' | sort
}

if [ $# -eq 0 ]
then
    _listAvailableFunctions
    exit
fi

"$@"
